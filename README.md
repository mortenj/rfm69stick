# rfm69stick


## Hardware

The rfm69stick is essentially a shrinkified Adafruit Feather 32u4 with RFM69HCW on it, and without any breakout pads. It features an SMA antenna connector, but a stub antenna can easily be soldered on instead.

The design of this USB stick came out of a need to interface to Cavius wireless fire alarms, but the hardware is generic and can be used for a wide variety of fun activities.

![](kicad/rfm69stick/rfm69stick.png)

## Firmware

The firmware is planned to provide a KISS TNC interface over USB serial, this means that the stick can be used for radio amateur activities as a side effect.

## Software

We mean to provide a Python library for configuring and communicating with the hardware, as well as a Python CLI tool to provide a usable interface when testing and experimenting.

The library is supposed to be the ground work for a future integration in Home Assistant to let users see the status of their Cavius fire alarms.
