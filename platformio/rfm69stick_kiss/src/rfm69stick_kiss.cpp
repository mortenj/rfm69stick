
/*
 * rfm69stick_KISS
 *
 * KISS firmware for the rfm69stick
 *
 * Based on:
 *  - https://github.com/adafruit/RadioHead/blob/master/examples/rf69/rf69_server/rf69_server.pde
 *  - https://github.com/PA7FRN/kissTnc
 *
 * Run through the blender by LA1FTA
 * 
 */

#include <Arduino.h>
#include <SPI.h>
#include <RH_RF69.h>

#define DEBUG false

#define SER_SPEED 115200
#define RF_FREQ 868.0
#define RF_POWER 14

// Adafruit Feather 32u4 pins
#define RFM_CS 8
#define RFM_IRQ 7
#define RFM_RST 4

#define RF_BUFLEN RH_RF69_MAX_MESSAGE_LEN

#define FEND  0xC0  //0A is \r
#define FESC  0xDB
#define TFEND 0xDC
#define TFESC 0xDD

#define KS_UNDEF    0
#define KS_GET_CMD  1
#define KS_GET_DATA 2
#define KS_ESCAPE   3

#define CMD_DATA_FRAME    0x00 //42 is "B"
#define CMD_TX_DELAY      0x01
#define CMD_P             0x02
#define CMD_SLOT_TIME     0x03
#define CMD_TX_TAIL       0x04
#define CMD_FULL_DUPLEX   0x05
#define CMD_SET_HARDWARE  0x06
#define MAX_KISS_CMD      0x07
#define CMD_UNDEF         0x80
#define MIN_KISS_CMD      0xFD
#define CMD_LOOPBACK_TEST 0xFE
#define CMD_RETURN        0xFF

RH_RF69 rf69(RFM_CS, RFM_IRQ); 

/*** variables ***/

byte _txbuf[RF_BUFLEN];
int  _txlen = 0;
bool _kissOutFrameEnd = true;
int  _kissInState = KS_UNDEF;
byte _kissCommand = CMD_UNDEF;
byte _tncParam;

/*** functions ***/

void debug(const char* s) {
  if(DEBUG) Serial.println(s);
}

void debug_byte(byte b) {
  if(DEBUG) Serial.print(b, HEX);
}

void led_toggle() {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}

void kiss_reset_packet_in() {
  _txlen = 0;
  _kissInState = KS_UNDEF;
}

void kiss_store_byte_in(byte newByte) {
  debug_byte(newByte);
  _txbuf[_txlen] = newByte;
  _txlen ++;
  if(_txlen > RF_BUFLEN) kiss_reset_packet_in();
}

void kiss_process_byte_out(byte newByte, boolean frameEnd) {
  if (_kissOutFrameEnd) {
    Serial.write(FEND);
    Serial.write((byte) CMD_DATA_FRAME);
    _kissOutFrameEnd = false;
  }
  if (newByte == FEND) {
    Serial.write(FESC);
    Serial.write(TFEND);
  }
  else if (newByte == FESC) {
    Serial.write(FESC);
    Serial.write(TFESC);
  }
  else {
    Serial.write(newByte);
  }
  if (frameEnd) {
    Serial.write(FEND);
    _kissOutFrameEnd = true;
  }
}

void kiss_process_packet_in() {
  debug("Got packet containing: ");
  // return packet contents to serial port for inspection
  char str[_txlen+1];
  memcpy(str, _txbuf, _txlen);
  str[_txlen + 1] = 0; // Null termination.
  debug(str);
  led_toggle();
  rf69.send(_txbuf, _txlen);
  rf69.waitPacketSent();
  led_toggle();
  kiss_reset_packet_in();
}

void kiss_process_byte_in(byte newByte) {
  debug_byte(newByte);
  switch (_kissInState) {
    case KS_UNDEF:
      if (newByte == FEND) {
        _kissCommand = CMD_UNDEF;
        _kissInState = KS_GET_CMD;
        debug("GET_CMD");
      }
      break;
    case KS_GET_CMD:
      if (newByte != FEND) {
        if (true || (newByte < MAX_KISS_CMD) || (newByte > MIN_KISS_CMD)) {
          _kissCommand = newByte;
          _kissInState = KS_GET_DATA;
          debug("GET DATA");
        }
        else {
          _kissCommand = CMD_UNDEF;
          _kissInState = KS_UNDEF;
          debug("ERROR - UNDEF");
        }
      }
      break;
    case KS_GET_DATA:
      if (newByte == FESC) {
        _kissInState = KS_ESCAPE;
        debug("ESCAPE");
      }
      else if (newByte == FEND) {
        kiss_process_packet_in();
        _kissCommand = CMD_UNDEF;
        _kissInState = KS_GET_CMD;
        debug("GET_CMD");
      }
      else {
        kiss_store_byte_in(newByte);
      }
      break;
    case KS_ESCAPE:
      if (newByte == TFEND) {
        kiss_store_byte_in(FEND); 
        _kissInState = KS_GET_DATA;
        debug("GET_DATA");
      }
      else if (newByte == TFESC) {
        kiss_store_byte_in(FESC); 
        _kissInState = KS_GET_DATA;
        debug("GET_DATA");
      }
      else {
        kiss_reset_packet_in();
        _kissCommand = CMD_UNDEF;
        debug("UNDEF");
      }
      break;
  }
}

// Resets the RFM module and waits until it is supposed to be usable again.
void radio_reset() {
  pinMode(RFM_RST, OUTPUT);
  digitalWrite(RFM_RST, HIGH);
  delay(100);
  digitalWrite(RFM_RST, LOW);
  delay(100);
}

void radio_configure() {
  if (!rf69.init())
    debug("init failed!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  if (!rf69.setFrequency(RF_FREQ))
    debug("setFrequency failed");

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(RF_POWER, true);
  
  // The encryption key has to be the same as the one in the client
  //uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  //                  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  //rf69.setEncryptionKey(key);
  
  #if 0
    // For compat with RFM69 Struct_send
    rf69.setModemConfig(RH_RF69::GFSK_Rb250Fd250);
    rf69.setPreambleLength(3);
    uint8_t syncwords[] = { 0x2d, 0x64 };
    rf69.setSyncWords(syncwords, sizeof(syncwords));
    rf69.setEncryptionKey((uint8_t*)"thisIsEncryptKey");
  #endif
}

void rfm_handle_in() {
  if (rf69.available()) {
    // Receive message   
    uint8_t buf[RF_BUFLEN];
    uint8_t len = sizeof(buf);
    if (rf69.recv(buf, &len)) {
      //RH_RF69::printBuffer("request: ", buf, len);
      for(int i = 0; i < len; i++) {
        kiss_process_byte_out(buf[i], (i == len-1));
      }
    }
  }
}

void serial_handle_in() {
  if (Serial.available() > 0) {
    kiss_process_byte_in(Serial.read());
  }
}

/*** entry points ***/

void setup() {
  // Ready the activity LED
  pinMode(LED_BUILTIN, OUTPUT);

  // Init serial port
  Serial.begin(SER_SPEED);
  // Wait for USB serial connection
  while (!Serial) ;

  // Reset the RFM module
  debug("reset radio...");
  radio_reset();

  // Initialize radio
  debug("init radio...");
  radio_configure();

  debug("setup done.");
}

void loop() {
  serial_handle_in();
  rfm_handle_in();
}
